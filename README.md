# login-page-with-one-tap-sign-in
One Tap and Sign in with google and also Sign in with Facebook implemented.
Traditional Login and Sign-up is also implemented.
All types of sign in methods are authenticated usign jwt tokens and verfied in the backend node server.

## API KEYS AND FB SDK APP CODE
1. Google api with access to people api data required.
2. Google OAuth 2.0 Client IDs and CLient Secret required.
3. Facebook Sdk App id required

## Site to refer create api keys and client id's for google and facebook
1 Google Apis - https://medium.com/analytics-vidhya/adding-sign-in-with-google-to-your-website-b82755b79b31 
2 Facebook SDK - https://dev.to/quod_ai/how-to-integrate-facebook-login-api-into-your-react-app-33de

## Config
1. Add google CLIENT_ID, CLIENT_SECRET & API_KEY in googleAuth.config.js file in server.
2. Add google CLIENT_ID to react client index.js file.
3. Add Facebook SDK App id inside index.html of client at line 33 in FB sdk initialization functoin.
4. Add db config like host, user, password in db.config.js in server.
5. Add jwt secret which is a random combination of characters and numbers here to make your secret key more secure. you can add your own combinations of characters.

## Run the App
**Open cmd in project folder and follow below commands.**
1. cd client
2. npm install
3. npm start
4. cd ..
5. cd server
6. npm install
7. npm start

## Things to remember
1. Make sure your api ids are correct.
2. Make sure DB is configured properly and running.
3. Make sure to npm install all libraries for both client and server before starting them for the first time.
4. Make sure disable adblock in your browser to avoid weird compatibility issues.
   
## CLIENT (React App)
# Getting Started with Create React App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in your browser.

The page will reload when you make changes.\
You may also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

### `npm run eject`

**Note: this is a one-way operation. Once you `eject`, you can't go back!**

If you aren't satisfied with the build tool and configuration choices, you can `eject` at any time. This command will remove the single build dependency from your project.

Instead, it will copy all the configuration files and the transitive dependencies (webpack, Babel, ESLint, etc) right into your project so you have full control over them. All of the commands except `eject` will still work, but they will point to the copied scripts so you can tweak them. At this point you're on your own.

You don't have to ever use `eject`. The curated feature set is suitable for small and middle deployments, and you shouldn't feel obligated to use this feature. However we understand that this tool wouldn't be useful if you couldn't customize it when you are ready for it.

## Learn More

You can learn more in the [Create React App documentation](https://facebook.github.io/create-react-app/docs/getting-started).

To learn React, check out the [React documentation](https://reactjs.org/).

### Code Splitting

This section has moved here: [https://facebook.github.io/create-react-app/docs/code-splitting](https://facebook.github.io/create-react-app/docs/code-splitting)

### Analyzing the Bundle Size

This section has moved here: [https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size](https://facebook.github.io/create-react-app/docs/analyzing-the-bundle-size)

### Making a Progressive Web App

This section has moved here: [https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app](https://facebook.github.io/create-react-app/docs/making-a-progressive-web-app)

### Advanced Configuration

This section has moved here: [https://facebook.github.io/create-react-app/docs/advanced-configuration](https://facebook.github.io/create-react-app/docs/advanced-configuration)

### Deployment

This section has moved here: [https://facebook.github.io/create-react-app/docs/deployment](https://facebook.github.io/create-react-app/docs/deployment)

### `npm run build` fails to minify

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
# login-page-with-one-tap-sign-in


